from django.urls import path
from . import views

urlpatterns = [
    path('', views.home, name='home'),
    path('djserver', views.djserver, name='djserver'),
    path('fskserver', views.fskserver, name='fskserver'),
    path('ndserver', views.ndserver, name='ndserver'),
    path('exserver', views.exserver, name='exserver'),
]