from django.shortcuts import render
import requests

# Create your views here.

djurl = 'http://djangoserver.pythonanywhere.com/'
fskurl = 'http://adhwaithk.pythonanywhere.com/'
exurl = 'http://expserverapi.herokuapp.com/'
nodeurl = 'http://hellonodew.herokuapp.com/'

def home(request):
    return render(request,'home.html')

def djserver(request):
    r = requests.get(djurl)
    resptime1 = r.elapsed
    return render(request,'home.html',{'dspeed':resptime1})

def fskserver(request):
    j = requests.get(fskurl)
    resptime2 = j.elapsed
    return render(request,'home.html',{'fspeed':resptime2})

def ndserver(request):
   k = requests.get(nodeurl)
   resptime3 = k.elapsed
   return render(request,'home.html',{'nspeed':resptime3})


def exserver(request):
    l = requests.get(exurl)
    resptime4 = l.elapsed
    return render(request,'home.html',{'espeed':resptime4})

