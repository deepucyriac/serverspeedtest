const express = require("express");
const app = express();
let port = process.env.PORT || 3000;

app.get("/",(req,res) =>{
    res.send("Welcome to Express server");
});

app.listen(port,() =>{
    console.log('Example app running');
});