# serversd

Server Speed Test

URL: http://serverspeed.pythonanywhere.com/

Working:
	This website provides a list of server built using different web frameworks and displays the response time of each web framework based on the user input. This is built using django web framework.
	The different web frameworks available for speed test are django, flask, expressjs and nodejs.
	
The URLs of the different servers are:

Django Server URL: http://djangoserver.pythonanywhere.com/

Flask Server URL: http://adhwaithk.pythonanywhere.com/

Expressjs Server URL: http://expserverapi.herokuapp.com/

Nodejs Server URL: http://hellonodew.herokuapp.com/



